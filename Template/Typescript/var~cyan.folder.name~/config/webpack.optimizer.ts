import UglifyJsPlugin from 'uglifyjs-webpack-plugin'; //flag!~build.es6~
import {Options} from "webpack";
const TerserPlugin = require('terser-webpack-plugin'); //flag~build.es6~

let opti : Options.Optimization = {};

//if!~build.es6~
opti  = {
	minimizer: [
		new UglifyJsPlugin({
			uglifyOptions: {
				compress: {
					drop_console: true,//flag!~build.console~
					unsafe: true //flag~build.unsafe~
				},
				output: {comments: false},//flag~build.stripComment~
				toplevel: true //flag~build.renameGlobal~
			}
		})
	]
};
//end!~build.es6~
//if~build.es6~
opti = {
	minimizer: [
		new TerserPlugin({
			terserOptions: {
				compress: {
					drop_console: true, //flag!~build.console~
					unsafe: true //flag~build.unsafe~
				},
				output: {comments: false}, //flag~build.stripComment~
				toplevel: true //flag~build.renameGlobal~
			}
		})
	]
};
//end~build.es6~

export {opti};