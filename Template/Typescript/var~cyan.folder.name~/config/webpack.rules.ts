import {RuleSetRule, RuleSetUseItem} from "webpack";
import * as path from "path"; //flag~build.babel~

/*===================
 TS LOADER
 ===================== */

let uses: RuleSetUseItem[] = [
	{loader: 'babel-loader', options: {configFile: path.resolve(__dirname, "./flag~build.babel~babel.config.js")}}, //flag~build.babel~
	{loader: 'ts-loader'}
];

let scripts: RuleSetRule = {
	test: /\.tsx?$/,
	exclude: /(node_modules|bower_components)/,
	use: uses
};


/*===================
 EXPORT
 ===================== */

let rules: RuleSetRule[] = [scripts];

export {rules};
