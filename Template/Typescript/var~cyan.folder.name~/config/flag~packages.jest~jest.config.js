module.exports = {
    "roots": [
        "<rootDir>"
    ],
    "transform":{
        "^.+\\.tsx?$": "ts-jest"
    },
    "moduleNameMapper": {
        "\\.(css|scss)$": "identity-obj-proxy"
    },
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
    "moduleFileExtensions": [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json",
        "node"
    ],
    collectCoverageFrom: [
        "**/src/**/*.ts",
        "!**/src/var~binary~.ts"
    ],

    "coverageThreshold": {
        "global": {
            "branches": 80,
            "functions": 80,
            "lines": 80,
            "statements": 100
        }
    }
};