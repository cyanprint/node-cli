import {Shape} from "./Shape";

class ShapeDataViewer {
	
	constructor() {}
	
	GetData(shape: Shape):string{
		return "area: "+shape.area.toString() +" parameter: "+ shape.parameter().toString();
	}
	
}

export {ShapeDataViewer};