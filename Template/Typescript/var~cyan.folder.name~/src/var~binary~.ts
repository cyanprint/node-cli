import {Core, Kore} from "@kirinnee/core"; //flag~move.@kirinnee/core~
import {Shape} from "./classLibrary/Shape";
import {Rectangle} from "./classLibrary/Rectangle";
import {ShapeDataViewer} from "./classLibrary/flag~mock~ShapeDataViewer";
import program from "commander"; //flag~packages.commander~
import * as inquirer from "inquirer"; //flag~packages.inquirer~
import chalk from "chalk";  //flag~packages.chalk~
import {ObjectX, Objex} from "@kirinnee/objex"; //flag~packages.@kirinnee/objex~

let core:Core = new Kore();//flag~move.@kirinnee/core~
core.ExtendPrimitives();//flag~move.@kirinnee/core~

let objex: Objex = new ObjectX(core); //flag~packages.@kirinnee/objex~
objex.ExtendPrimitives(); //flag~packages.@kirinnee/objex~

//if~packages.commander~
program
	.version("0.0.1")
	.description("var~description~");

program.parse(process.argv);
//end~packages.commander~

let dataView: ShapeDataViewer = new ShapeDataViewer(); //flag~mock~
let rect: Shape = new Rectangle(5,12);

//if~mock~
let print:string = dataView.GetData(rect);
print = chalk.cyan(print); //flag~packages.chalk~
console.log(print);
//end~mock~
//if!~mock~
let info:string = rect.area + " : " + rect.parameter();
print = chalk.cyan(info); //flag~packages.chalk~
console.log(info);
//end!~mock~

Program().then();

async function Program(){
	//if~packages.inquirer~
	let answer:any  = await inquirer.prompt([
		{
			type: "input",
			message: "What's your name~?",
			name: "name"
		}
	]);
	let name:string = answer.name;
	name = name.CapitalizeWords(); //flag~move.@kirinnee/core~
	name = chalk.redBright(name); //flag~packages.chalk~
	console.log("Hello",name,"!");
	//end~packages.inquirer~
	
	console.log("End of program~");
}