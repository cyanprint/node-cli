import {should} from 'chai';
import { stubInterface } from "ts-sinon";
import {ShapeDataViewer} from "../src/classLibrary/flag~mock~ShapeDataViewer";
import {Shape} from "../src/classLibrary/Shape";

should();

describe("ShapeDataViewer", () => {
	
	let dataview: ShapeDataViewer = new ShapeDataViewer();
	
	let circle: Shape = stubInterface<Shape>();
	circle.area = 40;
	(circle.parameter as any).returns(50);
	
	it("view data of shape", () => {
		dataview.GetData(circle).should.equal("area: 40 parameter: 50");
	});
});