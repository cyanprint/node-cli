

// @ts-ignore
import {Square} from "../src/classLibrary/Square";
import {Shape} from "../src/classLibrary/Shape";

test("Square", () => {
	let rect: Shape = new Square(7);
	
	expect(rect.area).toBe(49);
	expect(rect.parameter()).toBe(28);
});