

// @ts-ignore
import {Shape} from "../src/classLibrary/Shape";
import {Rectangle} from "../src/classLibrary/Rectangle";

test("Rectangle", () => {
	
	let rect: Shape = new Rectangle(5, 10);
	
	expect(rect.area).toBe(50);
	expect(rect.parameter()).toBe(30);
	
});