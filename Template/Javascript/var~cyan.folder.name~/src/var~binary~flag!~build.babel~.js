var Kore = require("@kirinnee/core").Kore; //flag~packages.@kirinnee/core~
var ObjectX = require("@kirinnee/objex").ObjectX; //flag~move.@kirinnee/objex~
var Rectangle = require("./classLibrary/Rectangle");
var program = require("commander");//flag~packages.commander~
var inquirer = require("inquirer"); //flag~packages.inquirer~
var chalk = require("chalk"); //flag~packages.chalk~

var core = new Kore();//flag~move.@kirinnee/core~
core.ExtendPrimitives();//flag~move.@kirinnee/core~

var objex = new ObjectX(core); //flag~packages.@kirinnee/objex~
objex.ExtendPrimitives(); //flag~packages.@kirinnee/objex~

//if~packages.commander~
program
    .version("0.0.1")
    .description("var~description~");

program.parse(process.argv);
//end~packages.commander~

var rect = new Rectangle(5, 12);
var print = rect.area() + " : " + rect.parameter();
print = chalk.cyan(print);//flag~packages.chalk~
console.log(print);


//if~packages.inquirer~
inquirer.prompt([
    {
        type: "input",
        message: "What's your name~?",
        name: "name"
    }
]).then(function(){
    var name = answer.name;
    name = name.CapitalizeWords(); //flag~move.@kirinnee/core~
    name = chalk.redBright(name); //flag~packages.chalk~
    console.log("Hello", name, "!");
});
//end~packages.inquirer~
