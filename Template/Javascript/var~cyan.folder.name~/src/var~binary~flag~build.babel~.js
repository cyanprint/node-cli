let Kore = require("@kirinnee/core").Kore; //flag~move.@kirinnee/core~
let Rectangle = require("./classLibrary/Rectangle");
let program = require("commander");//flag~packages.commander~
let inquirer = require("inquirer"); //flag~packages.inquirer~
let chalk = require("chalk"); //flag~packages.chalk~

let core = new Kore();//flag~move.@kirinnee/core~
core.ExtendPrimitives();//flag~move.@kirinnee/core~

//if~packages.commander~
program
    .version("0.0.1")
    .description("var~description~");

program.parse(process.argv);
//end~packages.commander~

let rect = new Rectangle(5, 12);
let print = rect.area() + " : " + rect.parameter();
print = chalk.cyan(print);//flag~packages.chalk~
console.log(print);

Program().then();

async function Program() {
    //if~packages.inquirer~
    let answer = await inquirer.prompt([
        {
            type: "input",
            message: "What's your name~?",
            name: "name"
        }
    ]);
    let name = answer.name;
    name = name.CapitalizeWords(); //flag~move.@kirinnee/core~
    name = chalk.redBright(name); //flag~packages.chalk~
    console.log("Hello", name, "!");
    //end~packages.inquirer~

    console.log("End of program~");
}