module.exports = function Rectangle(width, height) {
    this.width = width;
    this.height = height;


    this.area = function () {
        return this.width * this.height;
    };

    this.parameter = function () {
        return this.width * 2 + this.height * 2;
    };
};

