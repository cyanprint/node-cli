module.exports = function Square(side) {
    this.side = side;

    this.area = function () {
        return this.side * this.side;
    };

    this.parameter = function () {
        return this.side * 4;
    };

};