let path = require("path"); //flag~build.babel~

/*===================
 BABEL LOADER
 ===================== */

let uses = [
	{loader: 'babel-loader', options: {configFile: path.resolve(__dirname, "./flag~build.babel~babel.config.js")}} //flag~build.babel~
];

let scripts = {
	test: /\.tsx?$/,
	exclude: /(node_modules|bower_components)/,
	use: uses
};


/*===================
 EXPORT
 ===================== */

let rules = [scripts];
module.exports = rules;
