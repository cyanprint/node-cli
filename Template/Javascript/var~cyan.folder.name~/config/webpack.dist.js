let Kore = require("@kirinnee/core").Kore;
let rules = require("./webpack.rules");
let opti = require("./webpack.optimizer");
let path = require("path");
let core = new Kore();
let webpack = require("webpack");
core.ExtendPrimitives();

let entry = {
    "var~binary~flag~build.babel~": "./src/var~binary~flag~build.babel~.js",
    "var~binary~flag!~build.babel~": "./src/var~binary~flag!~build.babel~.js"
};

function GenerateConfig(entry, filename, mode) {
    let outDir = path.resolve(__dirname, "../dist");
    let config = {
        entry: entry,
        output: {
            path: outDir,
            filename: filename,
            libraryTarget: "umd",
            globalObject: "(typeof window !== 'undefined' ? window : this)"
        },
        plugins: [new webpack.BannerPlugin({banner: "#!/usr/bin/env node", raw: true})],
        resolve: {
            extensions: ['.ts', '.tsx', '.js']
        },
        mode: mode,
        devtool: "source-map", //flag~build.sourceMap~
        module: {rules: rules},
        target: "node",
        node: {__dirname: false, __filename: false}
    };
    if (mode === "production") config.optimization = opti;
    return config;
}

module.exports = [
    GenerateConfig(entry, '[name].min.js', 'production'),
    GenerateConfig(entry, '[name].js', 'development')
];