module.exports = {
    "roots": [
        "<rootDir>"
    ],
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.jsx?$",
    "moduleFileExtensions": [
        "js",
        "jsx",
        "json",
        "node"
    ],
    collectCoverageFrom: [
        "src/**/*.js",
        "!src/var~binary~flag~build.babel~.js",
        "!src/var~binary~flag!~build.babel~.js"
    ],

    "coverageThreshold": {
        "global": {
            "branches": 80,
            "functions": 80,
            "lines": 80,
            "statements": 100
        }
    }
};