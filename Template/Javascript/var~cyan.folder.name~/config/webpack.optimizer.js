const UglifyJsPlugin = require('uglifyjs-webpack-plugin'); //flag!~build.es6~
const TerserPlugin = require('terser-webpack-plugin'); //flag~build.es6~

let opti= {};

//if!~build.es6~
opti  = {
	minimizer: [
		new UglifyJsPlugin({
			uglifyOptions: {
				compress: {
					drop_console: true,//flag!~build.console~
					unsafe: true //flag~build.unsafe~
				},
				output: {comments: false},//flag~build.stripComment~
				toplevel: true //flag~build.renameGlobal~
			}
		})
	]
};
//end!~build.es6~
//if~build.es6~
opti = {
	minimizer: [
		new TerserPlugin({
			terserOptions: {
				compress: {
					drop_console: true, //flag!~build.console~
					unsafe: true //flag~build.unsafe~
				},
				output: {comments: false}, //flag~build.stripComment~
				toplevel: true //flag~build.renameGlobal~
			}
		})
	]
};
//end~build.es6~

module.exports = opti;