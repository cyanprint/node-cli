let Rectangle = require("../src/classLibrary/Rectangle");

test("Rectangle", () => {
	
	let rect = new Rectangle(5, 10);
	
	expect(rect.area()).toBe(50);
	expect(rect.parameter()).toBe(30);
	
});