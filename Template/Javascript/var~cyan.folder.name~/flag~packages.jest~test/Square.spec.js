let Square = require("../src/classLibrary/Square");

test("Square", () => {
	let rect = new Square(7);
	
	expect(rect.area()).toBe(49);
	expect(rect.parameter()).toBe(28);
});