require("chai").should();
let Square = require("../src/classLibrary/Square");

describe("Square", () => {

    let square = new Square(5);
    
    it("should return correct area", () => {
        square.area().should.be.equal(25);
    });

    it("should return correct parameter", () => {
        square.parameter().should.equal(20);
    });

});
