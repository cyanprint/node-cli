require("chai").should();
let Rectangle = require("../src/classLibrary/Rectangle");

describe("Rectangle", () => {
    let rect = new Rectangle(5, 10);
    it("should return correct area", () => {
        rect.area().should.be.equal(50);
    });

    it("should return correct parameter", () => {
        rect.parameter().should.equal(30);
    });
});