let spawn = require('child_process').spawn;
let rimraf = require("rimraf"); //flag~packages.nyc~
let fs = require("fs");

//command will be the first argument
let command = process.argv[2];
//args will be the array of arguments after the commands
let args = process.argv.slice(3).map(s => s.substr(2) === "--" ? s.substr(1) : s).filter(s => s !== '-w' && s !== '-watch');
//This is a boolean as to whether --watch or -w been in the commands
let watch = process.argv.map(s => s.substr(2) === "--" ? s.substr(1) : s).filter(s => s === '-w' || s === "-watch").length > 0;

Execute(command, args, watch).then();

//The decision on what command to execute base on the arguments and whether watch exists
async function Execute(command, args, watch) {
    let coverage = args.filter(s => s === "--cover").length > 0; //flag~cover~
    switch (command) {
        case "wp":
            let wp = `webpack --config ./config/webpack.${args[0]}.js`;
            if (watch) wp += " --watch";
            await run(wp);
            break;
        case "deploy":
            await run("node script flag~packages.chai~test --cover");
            await run("node script flag~packages.jest~test --cover");
            await run("node script wp dist");
            break;
        //if~packages.chai~
        case "flag~packages.chai~test":
            let mocha = `mocha --recursive ./flag~packages.chai~test/**/*.spec.js`;
            if (watch) mocha += " --watch ";
            if (coverage) mocha = "nyc --nycrc-path ./config/flag~packages.nyc~.nycrc " + mocha; //flag~cover~
            if (fs.existsSync("./.nyc_output")) rimraf.sync("./.nyc_output"); //flag~cover~
            await run(mocha);
            break;
        //end~packages.chai~
        //if~packages.jest~
        case "flag~packages.jest~test":
            let jest = "jest --rootDir --config=./config/flag~packages.jest~jest.config.js ./flag~packages.jest~test/";
            if (watch) jest += " --watch";
            if (coverage) jest += " --coverage";  //flag~cover~
            await run(jest);
            break;
        //end~packages.jest~
        case "publish":
            await run(`npm run deploy`);
            await run(`git add .`); //flag~cyan.docs.git~
            await run(`git commit -m "Preparing for next ${args[0]} version`); //flag~cyan.docs.git~
            await run(`npm version ${args[0]}`);
            await run(`npm publish`);
            break;
        default:
            console.log("Unknown command!");
            process.exit(1);
    }

}

//Executes the function as if its on the CMD. Exits the script if the external command crashes.
async function run(command) {
    if (!Array.isArray(command) && typeof command === "string") command = command.split(' ');
    else throw new Error("command is either a string or a string array");
    let c = command.shift();
    let v = command;

    let env = process.env;
    env.BROWSERSLIST_CONFIG= "./config/.browserslistrc";

    return new Promise((resolve) => spawn(c, v,
        {
            stdio: "inherit",
            shell: true,
            env: env
        })
        .on("exit", (code, signal) => {
            if (fs.existsSync("./.nyc_output")) rimraf.sync("./.nyc_output"); //flag~cover~
            if (code === 0) resolve();
            else {
                console.log("ExternalError:", signal);
                process.exit(1);
            }
        })
    );
}