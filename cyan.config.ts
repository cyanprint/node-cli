import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, IAutoInquire, IAutoMapper, IExecute} from "./Typings";

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {

    let isTypescript : boolean = await autoInquirer.InquirePredicate("Which language do you want to write in?", "Typescript", "Javascript");

    let subConfig: string = isTypescript ? "./cyan.typescript.js" : "./cyan.javascript.js";

    return execute.call(subConfig);
}

