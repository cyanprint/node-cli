import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, Documentation, DocUsage, Glob, IAutoInquire, IAutoMapper, IExecute} from "./Typings";

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {
	
	let ide: any = {
		vs2017: "VS 2017",
		webStorm: "WebStorm",
		editor: "Text Editor (VS Code, Sublime, Atom etc)"
	};
	
	let ideAnswer: object = await autoInquirer.InquireAsList(ide, "Which IDE do you use?");
	let ideChoice: string = autoMap.ReverseLoopUp(ide, ideAnswer);
	
	let globs: Glob[] = [{root: "./Template/Common/", pattern: "**/*.*", ignore: []}];
	let guid: string[] = [];
	let NPM: string | boolean = true;
	
	let ideIgnore: { vs2017: string[], webStorm: string[] } = {
		vs2017: ["**/.idea/**/*"],
		webStorm: ["**/obj/**/*", "**/*.njsproj", "**/*.njsproj.user"]
	};
	
	//Pick files base on IDE
	if (ideChoice === ide.webStorm) {
		globs.push({root: "./Template/Javascript/var~cyan.folder.name~", pattern: "**/*.*", ignore: ideIgnore.webStorm})
	} else if (ideChoice === ide.vs2017) {
		NPM = folderName;
		globs.push({root: "./Template/Javascript/", pattern: "**/*.*", ignore: ideIgnore.vs2017});
		guid.push("DC50B7F6-F769-4A0C-BF09-C8AF093FF897");
		guid.push("AE8FAFF8-C5EC-4664-9C33-722BACB087D7");
	} else {
		globs.push({
			root: "./Template/Javascript/var~cyan.folder.name~",
			pattern: "**/*.*",
			ignore: ideIgnore.vs2017.concat(ideIgnore.webStorm)
		})
	}
	//Ask for documentation
	let docQuestions: DocUsage = {license: false, git: false, readme: true, semVer: false, contributing: false};
	let docs: Documentation = await autoInquirer.InquireDocument(docQuestions);
	
	//Get read question to ask
	let variables: any = {
		name: {
			package: ["node-console-app", "Enter the name of your package"]
		},
		binary: ["cli", "Enter the name of binary (name of your command)"]
	};
	
	//Get answers
	variables = await autoInquirer.InquireInput(variables);
	
	//Fill in empty information if document already have them
	variables.name.author = docs.data.author;
	variables.email = docs.data.email;
	variables.description = docs.data.description;
	
	let flags: any = {
		unitTest: false,
		packages: {
			//babel
			"@babel/core": false,
			"@babel/plugin-proposal-object-rest-spread": false,
			"@babel/preset-env": false,
			"babel-loader": false,
			//test
			chai: false,
			mocha: false,
			jest: false,
			nyc: false,
			//minify
			"terser-webpack-plugin": false,
			"uglifyjs-webpack-plugin": true,
			//tools
			"graceful-fs": false,
			glob: false,
			inquirer:false,
			chalk: false,
			commander: false,
			"@kirinnee/objex": false
		},
		move: {
			"@kirinnee/core": false,
			"rimraf": false
		},
		cover: false,
		build: {
			babel: false,
			sourceMap: true,
			es6: false,
			console: false,
			unsafe: false,
			stripComment: false,
			renameGlobal: false
		}
	};
	
	
	//Ask for for features
	let features: any = {
		unitTest: "Unit Test",
		packages: {
			chalk: "Chalk",
			commander: "Commander js",
			inquirer: "Inquirer",
			glob: "Glob",
			"graceful-fs":"Graceful fs",
			"@kirinnee/objex": "Objex"
		},
		move: {
			"@kirinnee/core": "Kirinnee Core Library",
			"rimraf": "rimraf"
		}
	};
	
	
	features = await autoInquirer.InquireAsCheckBox(features, "Which of these tools do you need?");
	flags = autoMap.Overwrite(features, flags);
	
	
	//if unit test
	if (flags.unitTest) {
		
		let cover:boolean = await autoInquirer.InquirePredicate("Do you need test coverage?");
		flags.cover = cover;
		let mochaChai: any = {
			packages: {
				chai: true,
				mocha: true,
				nyc: cover,
			},
		};
		let jest: any = {
			packages: {
				jest: true
			},
		};
		let test: boolean = await autoInquirer.InquirePredicate("Which Test Framework do you want to use?", "Mocha + Chai", "Jest");
		flags = autoMap.Overwrite(test ? mochaChai : jest, flags);
		
	}
	
	delete flags.unitTest;
	
	//Ask for build pipeline
	let builds: any = {
		build: {
			babel: "Babel 7",
			sourceMap: "Source Map",
			es6: "Minify ES6 (Use Terser instead of Uglify)",
			console: "Keep console.log",
			unsafe: "Unsafe Compression",
			stripComment: "Strip Comments",
			renameGlobal: "Top Level Mangle"
		}
	};
	
	builds = await autoInquirer.InquireAsCheckBox(builds, "Which of this features do you want in your build pipeline?");
	flags = autoMap.Overwrite(builds, flags);
	

	if (flags.build.es6) {
		flags.packages["uglifyjs-webpack-plugin"] = false;
		flags.packages["terser-webpack-plugin"] = true;
	}
	
	
	let browserlist: boolean = false;
	
	
	if (flags.build.babel) {
		flags.packages["@babel/core"] = true;
		flags.packages["babel-loader"] = true;
		flags.packages["@babel/preset-env"] = true;
		flags.packages["@babel/plugin-proposal-object-rest-spread"] = true;
		browserlist = true;
	}
	
	if (!browserlist) {
		globs = globs.map(g => {
			g.ignore = (g.ignore as string[]).concat(["**/.browserslistrc"]);
			return g;
		});
	}
	
	if (docs.usage.git) variables.git = docs.data.gitURL;
	
	return {
		globs: globs,
		flags: flags,
		variable: variables,
		docs: docs,
		guid: guid,
		comments: ["//"],
		npm: NPM
		
	} as Cyan;
	
}